-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Des 2021 pada 09.05
-- Versi server: 10.4.20-MariaDB
-- Versi PHP: 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `niagahoster`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('1ecf09dcc4e614325dda17139e29c5a6', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1638241923, 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:7:\"created\";s:19:\"2015-12-26 07:13:14\";s:4:\"role\";s:1:\"0\";s:6:\"status\";s:1:\"1\";}'),
('517ab9699928c78881359077ee63081d', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 1517145980, 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:7:\"created\";s:19:\"2015-12-26 07:13:14\";s:4:\"role\";s:1:\"0\";s:6:\"status\";s:1:\"1\";}'),
('83f891a21cc9d7fedf1cf13d1c028d86', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1638241752, ''),
('c4636b0c9dc74e3b18e1022b249dc695', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36', 1638241753, ''),
('ce343f0077cc766a3dea960e4668eee5', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', 1517448607, 'a:6:{s:9:\"user_data\";s:0:\"\";s:7:\"user_id\";s:1:\"1\";s:8:\"username\";s:5:\"admin\";s:7:\"created\";s:19:\"2015-12-26 07:13:14\";s:4:\"role\";s:1:\"0\";s:6:\"status\";s:1:\"1\";}');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_activity`
--

CREATE TABLE `tb_activity` (
  `activity_id` int(11) NOT NULL,
  `activity_name` varchar(50) NOT NULL,
  `activity_content` text NOT NULL,
  `activity_text` varchar(255) NOT NULL,
  `activity_image` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_activity`
--

INSERT INTO `tb_activity` (`activity_id`, `activity_name`, `activity_content`, `activity_text`, `activity_image`, `created_date`, `updated_date`) VALUES
(2, 'Lorem Ipsum 1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in lorem vitae nibh elementum tristique. Proin congue tellus et nunc gravida efficitur. In eget consectetur mauris. Vestibulum dictum tempor elementum. Etiam vitae nibh magna. Ut malesuada enim velit, et gravida turpis posuere sed. Praesent blandit ligula quis ipsum eleifend rutrum. Fusce pharetra ex nisl, eget fringilla arcu ornare eu. Aliquam eget consectetur nisi. Sed egestas maximus lectus, eu scelerisque orci egestas vitae. Phasellus et fringilla risus, et euismod leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas ac enim sagittis, pharetra dolor nec, vehicula quam.</p>\r\n\r\n<p>Aliquam tellus magna, condimentum in facilisis vitae, egestas eu magna. Phasellus ex justo, sollicitudin non nunc ac, porta interdum quam. Suspendisse et ante eget mauris volutpat imperdiet ac nec turpis. Nunc bibendum lobortis odio, ut tempor nibh vulputate id. Sed fringilla nunc elit, quis fringilla augue mollis sit amet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis ornare massa a ultrices pulvinar.</p>\r\n\r\n<p>Curabitur feugiat quam id ante tristique efficitur. Duis ac ultrices nulla, vel ornare felis. Suspendisse a nulla suscipit, vestibulum turpis eget, egestas enim. Praesent pretium hendrerit erat, sed vestibulum mi porta a. Vivamus tristique in nisl sit amet consectetur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas ut arcu tincidunt, sodales risus vel, finibus ante. Integer elit elit, bibendum sed quam nec, tincidunt eleifend dolor. Etiam posuere placerat dignissim. Nullam dignissim imperdiet magna, nec iaculis lectus mattis eu. Ut rutrum nunc sed lobortis maximus. Pellentesque sed pharetra tellus.</p>\r\n\r\n<p>Suspendisse fringilla augue lorem, nec gravida ex consequat in. Curabitur elementum nec orci in pharetra. Sed ultrices lacus sit amet felis consectetur, sit amet blandit diam consequat. Sed in libero non sapien aliquet ultrices. Cras vulputate erat sit amet hendrerit pulvinar. Mauris ac enim et ante egestas sollicitudin id eget felis. Cras vulputate vitae neque vitae vestibulum. Nullam velit nulla, venenatis at ante vel, pellentesque tincidunt nibh. Etiam egestas ligula magna, cursus egestas dolor mollis non. Nam lacus ex, convallis in blandit quis, vehicula et lectus. Cras est sem, malesuada vitae aliquam ac, eleifend iaculis urna. Quisque ut placerat libero, sit amet rhoncus neque. Vivamus commodo convallis tristique. Phasellus sed enim euismod, euismod lorem eu, fringilla ex. Nullam hendrerit non metus vel elementum. Sed lacus purus, semper nec pulvinar non, tincidunt a ipsum.</p>\r\n\r\n<p>Nunc ex est, venenatis sed nisi quis, vestibulum volutpat justo. Pellentesque venenatis nibh enim, vel placerat libero interdum sed. Praesent aliquam mollis urna sed vehicula. Aenean vehicula orci nulla. Nulla fermentum vestibulum mi ac porttitor. Suspendisse nec auctor mauris. Sed odio est, bibendum nec augue sit amet, mollis interdum quam. Suspendisse sit amet interdum ex.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in lorem vitae nibh elementum tristique. Proin congue tellus et nunc gravida efficitur. In eget consectetur mauris. Vestibulum dictum tempor elementum. Etiam vitae nibh magna. Ut malesuada enim', 'no-photo.jpg', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(3, 'Lorem Ipsum 2', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in lorem vitae nibh elementum tristique. Proin congue tellus et nunc gravida efficitur. In eget consectetur mauris. Vestibulum dictum tempor elementum. Etiam vitae nibh magna. Ut malesuada enim velit, et gravida turpis posuere sed. Praesent blandit ligula quis ipsum eleifend rutrum. Fusce pharetra ex nisl, eget fringilla arcu ornare eu. Aliquam eget consectetur nisi. Sed egestas maximus lectus, eu scelerisque orci egestas vitae. Phasellus et fringilla risus, et euismod leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas ac enim sagittis, pharetra dolor nec, vehicula quam.</p>\r\n\r\n<p>Aliquam tellus magna, condimentum in facilisis vitae, egestas eu magna. Phasellus ex justo, sollicitudin non nunc ac, porta interdum quam. Suspendisse et ante eget mauris volutpat imperdiet ac nec turpis. Nunc bibendum lobortis odio, ut tempor nibh vulputate id. Sed fringilla nunc elit, quis fringilla augue mollis sit amet. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis ornare massa a ultrices pulvinar.</p>\r\n\r\n<p>Curabitur feugiat quam id ante tristique efficitur. Duis ac ultrices nulla, vel ornare felis. Suspendisse a nulla suscipit, vestibulum turpis eget, egestas enim. Praesent pretium hendrerit erat, sed vestibulum mi porta a. Vivamus tristique in nisl sit amet consectetur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas ut arcu tincidunt, sodales risus vel, finibus ante. Integer elit elit, bibendum sed quam nec, tincidunt eleifend dolor. Etiam posuere placerat dignissim. Nullam dignissim imperdiet magna, nec iaculis lectus mattis eu. Ut rutrum nunc sed lobortis maximus. Pellentesque sed pharetra tellus.</p>\r\n\r\n<p>Suspendisse fringilla augue lorem, nec gravida ex consequat in. Curabitur elementum nec orci in pharetra. Sed ultrices lacus sit amet felis consectetur, sit amet blandit diam consequat. Sed in libero non sapien aliquet ultrices. Cras vulputate erat sit amet hendrerit pulvinar. Mauris ac enim et ante egestas sollicitudin id eget felis. Cras vulputate vitae neque vitae vestibulum. Nullam velit nulla, venenatis at ante vel, pellentesque tincidunt nibh. Etiam egestas ligula magna, cursus egestas dolor mollis non. Nam lacus ex, convallis in blandit quis, vehicula et lectus. Cras est sem, malesuada vitae aliquam ac, eleifend iaculis urna. Quisque ut placerat libero, sit amet rhoncus neque. Vivamus commodo convallis tristique. Phasellus sed enim euismod, euismod lorem eu, fringilla ex. Nullam hendrerit non metus vel elementum. Sed lacus purus, semper nec pulvinar non, tincidunt a ipsum.</p>\r\n\r\n<p>Nunc ex est, venenatis sed nisi quis, vestibulum volutpat justo. Pellentesque venenatis nibh enim, vel placerat libero interdum sed. Praesent aliquam mollis urna sed vehicula. Aenean vehicula orci nulla. Nulla fermentum vestibulum mi ac porttitor. Suspendisse nec auctor mauris. Sed odio est, bibendum nec augue sit amet, mollis interdum quam. Suspendisse sit amet interdum ex.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in lorem vitae nibh elementum tristique. Proin congue tellus et nunc gravida efficitur. In eget consectetur mauris. Vestibulum dictum tempor elementum. Etiam vitae nibh magna. Ut malesuada enim', '1242826674.jpg', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, 'a', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique fringilla condimentum. Aliquam ut ex mauris. Vestibulum dolor dolor, blandit porta commodo eget, efficitur et libero. In maximus justo ultricies elementum suscipit. Proin eget mi vitae metus blandit blandit. Etiam at tellus id leo vehicula consectetur. Etiam posuere purus a purus accumsan, vel interdum sem iaculis. Donec mattis rutrum odio. Sed viverra maximus pulvinar. Morbi aliquam faucibus purus a porta. Proin vitae est luctus est iaculis laoreet ut ut est. Morbi in odio ut diam congue scelerisque.</p>\r\n\r\n<p>In hac habitasse platea dictumst. Quisque gravida justo a nunc pulvinar, vel tincidunt dui feugiat. Mauris fermentum nisi vitae nisl vulputate, sit amet scelerisque eros iaculis. Nam tristique tellus vitae est iaculis, sed tincidunt ex sodales. Mauris a egestas est. Sed pharetra, risus a rhoncus finibus, diam lorem cursus lacus, ac ornare leo libero ut augue. Fusce suscipit quam at odio lacinia, a vulputate lectus interdum. Sed imperdiet mattis metus, a eleifend risus viverra et. Curabitur luctus nisi et nibh fringilla tristique.</p>\r\n\r\n<p>Phasellus nec viverra odio. Praesent mi metus, sagittis quis libero ac, posuere aliquet mi. Sed eget pretium nulla. Duis pulvinar est non viverra fermentum. Suspendisse sollicitudin, velit vel cursus suscipit, diam nunc suscipit nisl, sit amet placerat dui felis in eros. In eget aliquet mauris, sit amet porta felis. Ut ac lectus ultricies, consequat erat quis, aliquet neque. Aliquam quis leo pellentesque, eleifend erat sed, malesuada ipsum. Ut dignissim vel massa et sollicitudin. Maecenas euismod vel elit a feugiat. In accumsan vulputate purus, et auctor neque consequat sit amet. Mauris id enim felis. Mauris sit amet ligula vitae dui gravida aliquam in sed ligula.</p>\r\n\r\n<p>Nullam tristique, nibh sed facilisis placerat, purus risus suscipit orci, et semper massa ex non ligula. Fusce interdum sapien feugiat rhoncus vehicula. Aliquam eu magna euismod, bibendum turpis non, vestibulum sem. Ut eros massa, egestas non pellentesque ut, mattis id augue. Vestibulum mollis velit ut dolor pretium convallis. Sed a pulvinar enim. Donec dignissim nisi non mi viverra, ut scelerisque magna tempus. Pellentesque ac consequat nibh. Morbi vulputate turpis justo, vel congue nulla venenatis sit amet. Maecenas ornare quis elit et feugiat. Vivamus ex metus, commodo pellentesque dui et, bibendum rutrum sem. Aenean non euismod tortor, eu suscipit purus. Pellentesque consequat eros et sapien rutrum tempor a sit amet risus. Aenean et molestie enim, eu mollis elit. Maecenas elementum ullamcorper nulla ac pulvinar. Sed ante odio, tempus nec dolor ac, pellentesque tincidunt mi.</p>\r\n\r\n<p>Cras quis ullamcorper sapien. Duis feugiat dui et augue convallis dignissim. Nulla mollis dapibus ex a maximus. Proin eget imperdiet justo. Ut tortor augue, sollicitudin vitae vulputate eget, hendrerit at dui. Sed pellentesque interdum mattis. Vestibulum tincidunt tempor massa sit amet ornare.</p>', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tristique fringilla condimentum. Aliquam ut ex mauris. Vestibulum dolor dolor, blandit porta commodo eget, efficitur et libero. In maximus justo ultricies elementum suscipit. Proin eget', 'no-photo.jpg', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(6, 'a', '<p>test</p>', 'a', 'no-photo.jpg', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(7, 'a', '<p>a</p>', 'a', 'no-photo.jpg', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_article`
--

CREATE TABLE `tb_article` (
  `article_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `article_tags` text DEFAULT NULL,
  `article_name` text DEFAULT NULL,
  `article_content` text DEFAULT NULL,
  `article_summary` text DEFAULT NULL,
  `article_image` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_article`
--

INSERT INTO `tb_article` (`article_id`, `categories_id`, `article_tags`, `article_name`, `article_content`, `article_summary`, `article_image`, `created_date`, `updated_date`) VALUES
(1, 1, '', 'Banner', 'Solusi PHP untuk performa query yang lebih cepat,\nKonsumsi memory yang lebih rendah,\nSupport PHP 5.3. PHP 5.4. PHP 5.5. PHP 5.6. PHP 7,\nFitur enkripsi IonCube dan Zend Guard Loaders', '<h2 class=\"montserrat-black\"> PHP Hosting </h2>\n<h3 class=\"montserrat\">Cepat, handal, penuh dengan modul PHP yang Anda butuhkan</h3>', 'svg/illustration banner PHP hosting-01.svg', '2021-12-06 15:46:22', '2021-12-06 15:46:22'),
(2, 1, '', 'Mendukung Penuh Framework Laravel', 'Install Laravel <b> 1 Klik </b> dengan Softculous Installer,\r\nMendukung ekstensi <b>PHP MCrype. phar. mbstring. json.</b> dan <b>fileinfo.</b>,\r\nTersedia <b>Composer</b> dan <b>SSH</b> untuk menginstall packages pilihan Anda.<br/><br/>\r\n<small>Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis', 'Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda', '/svg/illustration banner support laravel hosting.svg', '2021-12-07 07:23:47', '2021-12-07 07:23:47'),
(3, 1, '', 'Linux Hosting yang Stabil dengan Teknologi LVE', 'SuperMicro <b>Intel Xeon 24-Cores</b> server dengan RAM <b>128 GB</b> dan teknologi <b>LVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi dengan <b>SSD</b> untuk kecepatan <b>MySQL</b> dan caching, Apache load balancer berbasis LiteSpeed Technologies, <b>CageFS</b> security, <b>Raid-10</b> protection dan auto backup untuk keamanan website PHP Anda.', '', '/images/Image support.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(4, 4, '', 'Knowledgebase', '', '', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, 4, ' ', 'Blog', ' ', ' ', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(6, 4, NULL, 'Cara Pembayaran', NULL, NULL, NULL, NULL, NULL),
(7, 5, NULL, 'Tim Niagahoster', NULL, NULL, NULL, NULL, NULL),
(8, 5, NULL, 'Karir', NULL, NULL, NULL, NULL, NULL),
(9, 5, NULL, 'Events', NULL, NULL, NULL, NULL, NULL),
(10, 5, NULL, 'Penawaran & Promo Spesial', NULL, NULL, NULL, NULL, NULL),
(11, 5, NULL, 'Kontak Kami', NULL, NULL, NULL, NULL, NULL),
(12, 6, NULL, 'Support Terbaik', NULL, NULL, NULL, NULL, NULL),
(13, 6, NULL, 'Garansi Harga Termurah', NULL, NULL, NULL, NULL, NULL),
(14, 6, NULL, 'Domain Gratis Selamanya', NULL, NULL, NULL, NULL, NULL),
(15, 6, NULL, 'Datacenter Hosting Terbaik', NULL, NULL, NULL, NULL, NULL),
(16, 6, NULL, 'Review Pelanggan', NULL, NULL, NULL, NULL, NULL),
(17, 7, NULL, 'Newsletter', NULL, NULL, '/images/newsletter.png', NULL, NULL),
(18, 8, NULL, 'Share', NULL, NULL, '/images/share.png', NULL, NULL),
(19, 9, NULL, 'Pembayaran', NULL, NULL, '/images/pembayaran.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_categories`
--

CREATE TABLE `tb_categories` (
  `categories_id` int(11) NOT NULL,
  `categories_name` varchar(50) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_categories`
--

INSERT INTO `tb_categories` (`categories_id`, `categories_name`, `created_date`, `updated_date`) VALUES
(1, 'Home', '2021-12-07 10:26:22', '2021-12-09 10:20:00'),
(4, 'TUTORIAL', '2021-12-09 10:20:00', '2021-12-09 10:20:00'),
(5, 'TENTANG KAMI', '2021-12-09 10:20:00', '2021-12-09 10:20:00'),
(6, 'KENAPA PILIH NIAGAHOSTER?', '2021-12-09 10:20:00', '2021-12-09 10:20:00'),
(7, 'NEWSLETTER', NULL, NULL),
(8, ' ', NULL, NULL),
(9, 'PEMBAYARAN', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_category`
--

CREATE TABLE `tb_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_desc` text NOT NULL,
  `category_image` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_category`
--

INSERT INTO `tb_category` (`category_id`, `category_name`, `category_desc`, `category_image`, `created_date`, `updated_date`) VALUES
(1, 'Domain', '', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(3, 'Shared Hosting', '', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(4, 'Cloud VPS Hosting', '', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, 'Managed VPS Hosting', ' ', ' ', '2021-12-09 08:00:26', '2021-12-09 08:00:26'),
(6, 'Web Builder', ' ', ' ', '2021-12-09 08:00:26', '2021-12-09 08:00:26'),
(7, 'Keamanan SSL/HTTPS', ' ', ' ', '2021-12-09 08:00:26', '2021-12-09 08:00:26'),
(8, 'Jasa Pembuatan Website', ' ', ' ', '2021-12-09 08:00:26', '2021-12-09 08:00:26'),
(9, 'Program Affiliasi', ' ', ' ', '2021-12-09 08:00:26', '2021-12-09 08:00:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_client`
--

CREATE TABLE `tb_client` (
  `client_id` int(11) NOT NULL,
  `client_name` varchar(50) NOT NULL,
  `client_link` text NOT NULL,
  `client_image` text NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_client`
--

INSERT INTO `tb_client` (`client_id`, `client_name`, `client_link`, `client_image`, `created_date`, `updated_date`) VALUES
(1, '-', '-', '1042988755.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(2, '-', '-', '2550697249.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(3, '-', '-', '737076591.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(7, '-', '-', '2885453204.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(8, '-', '-', '4168731094.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(9, '-', '-', '4031755100.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(10, '-', '-', '2571007634.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(11, '-', '-', '123396913.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(12, '-', '-', '723154155.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(13, '-', '-', '2869433360.png', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_contact`
--

CREATE TABLE `tb_contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(25) NOT NULL,
  `contact_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_contact`
--

INSERT INTO `tb_contact` (`contact_id`, `contact_name`, `contact_value`) VALUES
(1, 'Email', 'rezamuzay@gmail.com'),
(2, 'Other Email', ''),
(3, 'Phone', '085813414927'),
(4, 'Other Phone', ''),
(5, 'Location', ''),
(6, 'Address', 'Jakarta Selatan, DKI Jakarta 12780, Indonesia'),
(7, 'Latitude', ''),
(8, 'Longitude', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_cp`
--

CREATE TABLE `tb_cp` (
  `cp_id` int(11) UNSIGNED ZEROFILL NOT NULL,
  `cp_name` varchar(50) NOT NULL,
  `cp_number` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_domain`
--

CREATE TABLE `tb_domain` (
  `domain_id` int(11) NOT NULL,
  `domain_name` varchar(10) NOT NULL,
  `domain_new` int(11) NOT NULL,
  `domain_extend` int(11) NOT NULL,
  `domain_transfer` int(11) NOT NULL,
  `domain_link` text NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_domain`
--

INSERT INTO `tb_domain` (`domain_id`, `domain_name`, `domain_new`, `domain_extend`, `domain_transfer`, `domain_link`, `created_date`, `updated_date`) VALUES
(4, '.com', 130000, 130000, 130000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, '.co.id', 130000, 130000, 130000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(6, '.org', 160000, 160000, 160000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(7, '.info', 49000, 152000, 152000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(8, '.net', 160000, 160000, 160000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(9, '.asia', 130000, 130000, 130000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(10, '.biz', 130000, 130000, 130000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(11, '.in', 130000, 130000, 130000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(12, '.co', 49000, 152000, 152000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(13, '.or.id', 130000, 130000, 130000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(14, '.web.id', 49000, 152000, 152000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(15, '.my.id', 49000, 152000, 152000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(16, '.ac.id', 158000, 158000, 158000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(17, '.sch.id', 130000, 130000, 130000, '', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_faq`
--

CREATE TABLE `tb_faq` (
  `faq_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `faq_title` varchar(100) NOT NULL,
  `faq_desc` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_faq`
--

INSERT INTO `tb_faq` (`faq_id`, `category_id`, `faq_title`, `faq_desc`, `created_date`, `updated_date`) VALUES
(2, 1, 'what is a shared hosting?', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates illum aspernatur odio simili', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(4, 1, 'how do i transfer web pages to server?', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates illum aspernatur odio simili', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, 1, 'purchased a hosting, now what do i do?', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates illum aspernatur odio simili', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(6, 2, 'how do i transfer web pages to server?', '<p>lorem ipsum</p>', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_feedback`
--

CREATE TABLE `tb_feedback` (
  `feedback_id` int(11) NOT NULL,
  `feedback_name` varchar(100) NOT NULL,
  `feedback_email` text NOT NULL,
  `feedback_company` varchar(100) NOT NULL,
  `feedback_website` text NOT NULL,
  `feedback_image` varchar(255) NOT NULL,
  `feedback_content` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_feedback`
--

INSERT INTO `tb_feedback` (`feedback_id`, `feedback_name`, `feedback_email`, `feedback_company`, `feedback_website`, `feedback_image`, `feedback_content`, `created_date`) VALUES
(1, 'Reza', '', 'PT', '', '', 'Mantap', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_image`
--

CREATE TABLE `tb_image` (
  `image_id` int(11) NOT NULL,
  `image_name` varchar(50) NOT NULL,
  `image_desc` varchar(100) NOT NULL,
  `image_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_image`
--

INSERT INTO `tb_image` (`image_id`, `image_name`, `image_desc`, `image_value`) VALUES
(1, 'banner', 'Banner', 'illustration banner PHP hosting-01.svg'),
(2, 'zenguard', 'PHP Zend Guard Loader', 'illustration-banner-PHP-zenguard01.svg'),
(3, 'composer', 'PHP Composer', 'icon-composer.svg'),
(4, 'ioncube', 'PHP ionCube Loader', 'icon-php-hosting-ioncube.svg'),
(5, 'bagikan', 'Share FB, Twitter, G+', 'bagikan.png'),
(6, 'tag', 'Top Header', 'tag.png'),
(7, 'navbar', 'Navbar', 'niagahoster.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_info`
--

CREATE TABLE `tb_info` (
  `info_id` int(11) NOT NULL,
  `info_title` varchar(15) NOT NULL,
  `info_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_info`
--

INSERT INTO `tb_info` (`info_id`, `info_title`, `info_value`) VALUES
(1, 'Owner', 'Reza Muzay'),
(2, 'Company', 'Niagahoster'),
(3, 'Language', 'indonesian'),
(4, 'Created By', 'Niagahoster'),
(5, 'Favicon', ''),
(6, 'Description', ''),
(7, 'Contact Us', '0274-5305505<br/>Senin - Minggu<br/>24 Jam Nonstop<br/></br>Jl. Selokan Mataram Monjali<br/>Karangjati MT 1/304<br/>Sinduadi, Mlati, Sleman<br/>Yogyakarta 55284');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_join`
--

CREATE TABLE `tb_join` (
  `join_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `join_value` varchar(20) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_join`
--

INSERT INTO `tb_join` (`join_id`, `price_id`, `product_id`, `join_value`, `created_date`, `updated_date`) VALUES
(1, 3, 1, '', '2017-02-06 08:00:56', NULL),
(2, 3, 2, '', '2017-02-06 08:00:56', NULL),
(6, 4, 2, 'A', '2017-02-06 08:10:39', NULL),
(7, 4, 3, 'A', '2017-02-06 08:10:39', NULL),
(8, 4, 4, 'A', '2017-02-06 08:10:39', NULL),
(9, 5, 1, 'X', '2017-02-07 06:14:09', NULL),
(10, 5, 2, 'Unlimited', '2017-02-07 06:14:09', NULL),
(11, 5, 3, 'X', '2017-02-07 06:14:09', NULL),
(12, 5, 4, 'Unlimited', '2017-02-07 06:14:09', NULL),
(70, 21, 1, 'GRATIS', '2017-03-15 18:44:24', NULL),
(71, 21, 2, 'GRATIS', '2017-03-15 18:44:24', NULL),
(72, 21, 3, 'GRATIS', '2017-03-15 18:44:24', NULL),
(73, 21, 4, 'GRATIS', '2017-03-15 18:44:24', NULL),
(74, 22, 1, '500 MB', '2017-03-15 18:45:03', NULL),
(75, 22, 2, '800 MB', '2017-03-15 18:45:03', NULL),
(76, 22, 3, '1536 MB', '2017-03-15 18:45:03', NULL),
(77, 22, 4, '5120 MB', '2017-03-15 18:45:03', NULL),
(78, 23, 1, 'UNLIMITED', '2017-03-15 18:47:21', NULL),
(79, 23, 2, 'UNLIMITED', '2017-03-15 18:47:21', NULL),
(80, 23, 3, '1024 MB', '2017-03-15 18:47:21', NULL),
(81, 23, 4, 'UNLIMITED', '2017-03-15 18:47:22', NULL),
(82, 24, 1, 'UNLIMITED', '2017-03-15 18:47:55', NULL),
(83, 24, 2, 'UNLIMITED', '2017-03-15 18:47:55', NULL),
(84, 24, 3, 'UNLIMITED', '2017-03-15 18:47:55', NULL),
(85, 24, 4, 'UNLIMITED', '2017-03-15 18:47:55', NULL),
(86, 25, 1, '1 Tahun', '2017-03-15 18:48:24', NULL),
(87, 25, 2, '1 Tahun', '2017-03-15 18:48:24', NULL),
(88, 25, 3, '1 Tahun', '2017-03-15 18:48:24', NULL),
(89, 25, 4, '1 Tahun', '2017-03-15 18:48:24', NULL),
(90, 26, 1, 'UNLIMITED', '2017-03-15 18:48:50', NULL),
(91, 26, 2, 'UNLIMITED', '2017-03-15 18:48:50', NULL),
(92, 26, 3, 'UNLIMITED', '2017-03-15 18:48:50', NULL),
(93, 26, 4, 'UNLIMITED', '2017-03-15 18:48:50', NULL),
(94, 27, 1, 'UNLIMITED', '2017-03-15 18:49:24', NULL),
(95, 27, 2, 'UNLIMITED', '2017-03-15 18:49:24', NULL),
(96, 27, 3, 'UNLIMITED', '2017-03-15 18:49:25', NULL),
(97, 27, 4, 'UNLIMITED', '2017-03-15 18:49:25', NULL),
(98, 28, 1, 'x', '2017-03-15 18:49:55', NULL),
(99, 28, 2, 'UNLIMITED', '2017-03-15 18:49:55', NULL),
(100, 28, 3, 'UNLIMITED', '2017-03-15 18:49:55', NULL),
(101, 28, 4, 'UNLIMITED', '2017-03-15 18:49:55', NULL),
(102, 29, 5, 'GRATIS', '2017-03-15 18:55:18', NULL),
(103, 29, 6, 'GRATIS', '2017-03-15 18:55:18', NULL),
(104, 29, 7, 'GRATIS', '2017-03-15 18:55:18', NULL),
(105, 29, 8, 'GRATIS', '2017-03-15 18:55:18', NULL),
(106, 30, 5, '500 MB', '2017-03-15 18:55:49', NULL),
(107, 30, 6, '1 GB', '2017-03-15 18:55:49', NULL),
(108, 30, 7, '2 GB', '2017-03-15 18:55:49', NULL),
(109, 30, 8, '4 GB', '2017-03-15 18:55:49', NULL),
(110, 31, 5, 'UNLIMITED', '2017-03-15 18:56:12', NULL),
(111, 31, 6, 'UNLIMITED', '2017-03-15 18:56:12', NULL),
(112, 31, 7, 'UNLIMITED', '2017-03-15 18:56:12', NULL),
(113, 31, 8, 'UNLIMITED', '2017-03-15 18:56:12', NULL),
(114, 32, 5, 'UNLIMITED', '2017-03-15 18:56:30', NULL),
(115, 32, 6, 'UNLIMITED', '2017-03-15 18:56:30', NULL),
(116, 32, 7, 'UNLIMITED', '2017-03-15 18:56:30', NULL),
(117, 32, 8, 'UNLIMITED', '2017-03-15 18:56:30', NULL),
(118, 33, 5, '1 Tahun', '2017-03-15 18:56:49', NULL),
(119, 33, 6, '1 Tahun', '2017-03-15 18:56:49', NULL),
(120, 33, 7, '1 Tahun', '2017-03-15 18:56:49', NULL),
(121, 33, 8, '1 Tahun', '2017-03-15 18:56:49', NULL),
(122, 34, 5, 'Max 15', '2017-03-15 18:57:20', NULL),
(123, 34, 6, 'UNLIMITED', '2017-03-15 18:57:20', NULL),
(124, 34, 7, 'UNLIMITED', '2017-03-15 18:57:20', NULL),
(125, 34, 8, 'UNLIMITED', '2017-03-15 18:57:20', NULL),
(126, 35, 5, 'Max 6', '2017-03-15 18:57:46', NULL),
(127, 35, 6, 'UNLIMITED', '2017-03-15 18:57:46', NULL),
(128, 35, 7, 'UNLIMITED', '2017-03-15 18:57:46', NULL),
(129, 35, 8, 'UNLIMITED', '2017-03-15 18:57:46', NULL),
(130, 36, 5, 'x', '2017-03-15 18:58:44', NULL),
(131, 36, 6, 'UNLIMITED', '2017-03-15 18:58:44', NULL),
(132, 36, 7, 'UNLIMITED', '2017-03-15 18:58:44', NULL),
(133, 36, 8, 'UNLIMITED', '2017-03-15 18:58:44', NULL),
(134, 37, 5, '700.000', '2017-03-15 18:59:23', NULL),
(135, 37, 6, '1.000.000', '2017-03-15 18:59:23', NULL),
(136, 37, 7, '1.800.000', '2017-03-15 18:59:23', NULL),
(137, 37, 8, '2.200.000', '2017-03-15 18:59:23', NULL),
(138, 38, 5, 'Vat 10%', '2017-03-15 19:00:21', NULL),
(139, 38, 6, 'Vat 10%', '2017-03-15 19:00:21', NULL),
(140, 38, 7, 'Vat 10%', '2017-03-15 19:00:21', NULL),
(141, 38, 8, 'Vat 10%', '2017-03-15 19:00:22', NULL),
(146, 40, 9, 'GRATIS', '2017-03-15 19:03:24', NULL),
(147, 40, 10, 'GRATIS', '2017-03-15 19:03:24', NULL),
(148, 40, 11, 'GRATIS', '2017-03-15 19:03:24', NULL),
(149, 40, 12, 'GRATIS', '2017-03-15 19:03:24', NULL),
(150, 41, 9, '10 GB', '2017-03-15 19:03:51', NULL),
(151, 41, 10, '12 GB', '2017-03-15 19:03:51', NULL),
(152, 41, 11, '23 GB', '2017-03-15 19:03:51', NULL),
(153, 41, 12, '50 GB', '2017-03-15 19:03:51', NULL),
(154, 42, 9, '128 MB', '2017-03-15 19:04:31', NULL),
(155, 42, 10, '512 MB', '2017-03-15 19:04:31', NULL),
(156, 42, 11, '1 GB', '2017-03-15 19:04:31', NULL),
(157, 42, 12, '50 GB', '2017-03-15 19:04:31', NULL),
(158, 43, 9, 'Linux/UNIX', '2017-03-15 19:04:59', NULL),
(159, 43, 10, 'Linux/UNIX', '2017-03-15 19:04:59', NULL),
(160, 43, 11, 'Linux/UNIX', '2017-03-15 19:04:59', NULL),
(161, 43, 12, 'Linux/UNIX', '2017-03-15 19:04:59', NULL),
(162, 44, 9, '1 Dedicated', '2017-03-15 19:05:31', NULL),
(163, 44, 10, '1 Dedicated', '2017-03-15 19:05:31', NULL),
(164, 44, 11, '1 Dedicated', '2017-03-15 19:05:31', NULL),
(165, 44, 12, '1 Dedicated', '2017-03-15 19:05:31', NULL),
(166, 45, 9, '380.000', '2017-03-15 19:06:14', NULL),
(167, 45, 10, '730.000', '2017-03-15 19:06:14', NULL),
(168, 45, 11, '1.200.000', '2017-03-15 19:06:14', NULL),
(169, 45, 12, '2.000.000', '2017-03-15 19:06:14', NULL),
(170, 46, 9, 'Vat 10%', '2017-03-15 19:06:32', NULL),
(171, 46, 10, 'Vat 10%', '2017-03-15 19:06:32', NULL),
(172, 46, 11, 'Vat 10%', '2017-03-15 19:06:32', NULL),
(173, 46, 12, 'Vat 10%', '2017-03-15 19:06:32', NULL),
(174, 47, 13, 'Indonesia', '2017-03-15 19:07:16', NULL),
(175, 47, 14, 'Indonesia', '2017-03-15 19:07:16', NULL),
(176, 47, 15, 'Indonesia', '2017-03-15 19:07:16', NULL),
(177, 47, 16, 'Indonesia', '2017-03-15 19:07:16', NULL),
(178, 48, 13, '1 Dedicated', '2017-03-15 19:07:45', NULL),
(179, 48, 14, '2 Dedicated', '2017-03-15 19:07:45', NULL),
(180, 48, 15, '3 Dedicated', '2017-03-15 19:07:45', NULL),
(181, 48, 16, '5 Dedicated', '2017-03-15 19:07:45', NULL),
(182, 49, 13, '100 MBPS', '2017-03-15 19:08:23', NULL),
(183, 49, 14, '100 MBPS', '2017-03-15 19:08:23', NULL),
(184, 49, 15, '100 MBPS', '2017-03-15 19:08:23', NULL),
(185, 49, 16, '100 MBPS', '2017-03-15 19:08:23', NULL),
(186, 50, 13, '15 MBPS', '2017-03-15 19:08:40', NULL),
(187, 50, 14, '15 MBPS', '2017-03-15 19:08:40', NULL),
(188, 50, 15, '15 MBPS', '2017-03-15 19:08:40', NULL),
(189, 50, 16, '15 MBPS', '2017-03-15 19:08:40', NULL),
(190, 51, 13, 'Max 600 Watt', '2017-03-15 19:08:54', NULL),
(191, 51, 14, 'Max 600 Watt', '2017-03-15 19:08:54', NULL),
(192, 51, 15, 'Max 600 Watt', '2017-03-15 19:08:54', NULL),
(193, 51, 16, 'Max 600 Watt', '2017-03-15 19:08:54', NULL),
(194, 52, 13, 'Max 2 U', '2017-03-15 19:09:20', NULL),
(195, 52, 14, 'Max 2 U', '2017-03-15 19:09:20', NULL),
(196, 52, 15, 'Max 4 U', '2017-03-15 19:09:20', NULL),
(197, 52, 16, 'Max 6 U', '2017-03-15 19:09:20', NULL),
(198, 53, 13, '1.500.000', '2017-03-15 19:09:54', NULL),
(199, 53, 14, '800.000', '2017-03-15 19:09:54', NULL),
(200, 53, 15, '500.000', '2017-03-15 19:09:54', NULL),
(201, 53, 16, '500.000', '2017-03-15 19:09:54', NULL),
(202, 54, 13, '3.500.000', '2017-03-15 19:10:27', NULL),
(203, 54, 14, '4.300.000', '2017-03-15 19:10:27', NULL),
(204, 54, 15, '5.100.000', '2017-03-15 19:10:27', NULL),
(205, 54, 16, '9.500.000', '2017-03-15 19:10:27', NULL),
(206, 55, 13, 'Vat 10%', '2017-03-15 19:10:39', NULL),
(207, 55, 14, 'Vat 10%', '2017-03-15 19:10:40', NULL),
(208, 55, 15, 'Vat 10%', '2017-03-15 19:10:40', NULL),
(209, 55, 16, 'Vat 10%', '2017-03-15 19:10:40', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_member`
--

CREATE TABLE `tb_member` (
  `member_id` int(11) NOT NULL,
  `member_name` varchar(100) NOT NULL,
  `member_image` text NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_member`
--

INSERT INTO `tb_member` (`member_id`, `member_name`, `member_image`, `created_date`, `updated_date`) VALUES
(1, 'Test 3', '984407275.jpg', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_message`
--

CREATE TABLE `tb_message` (
  `message_id` int(11) NOT NULL,
  `message_name` varchar(50) NOT NULL,
  `message_email` text NOT NULL,
  `message_subject` varchar(50) NOT NULL,
  `message_text` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_news`
--

CREATE TABLE `tb_news` (
  `news_id` int(11) NOT NULL,
  `news_email` text NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_newsletter`
--

CREATE TABLE `tb_newsletter` (
  `newsletter_id` int(11) NOT NULL,
  `newsletter_email` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_newsletter`
--

INSERT INTO `tb_newsletter` (`newsletter_id`, `newsletter_email`, `created_date`, `updated_date`) VALUES
(1, 'rezamuzay@gmail.com', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_page`
--

CREATE TABLE `tb_page` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(50) NOT NULL,
  `page_desc` text NOT NULL,
  `page_order` int(2) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_page`
--

INSERT INTO `tb_page` (`page_id`, `page_title`, `page_desc`, `page_order`, `created_date`, `updated_date`) VALUES
(6, 'About Us', '', 1, '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(7, 'Terms Of Service', '', 2, '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(8, 'Privacy', '', 3, '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_price`
--

CREATE TABLE `tb_price` (
  `price_id` int(11) NOT NULL,
  `price_name` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_price`
--

INSERT INTO `tb_price` (`price_id`, `price_name`, `category_id`, `created_date`, `updated_date`) VALUES
(1, 'MySQL / PostgreSQL D', 0, '2021-12-07 10:26:22', NULL),
(24, 'Email POP', 1, '2021-12-07 10:26:22', NULL),
(25, 'Minimum Kontrak', 1, '2021-12-07 10:26:22', NULL),
(26, 'Email Accounts', 1, '2021-12-07 10:26:22', NULL),
(29, 'Setup', 2, '2021-12-07 10:26:22', NULL),
(21, 'Setup', 1, '2021-12-07 10:26:22', NULL),
(22, 'Kapasitas', 1, '2021-12-07 10:26:22', NULL),
(23, 'Data Transfer (DN)', 1, '2021-12-07 10:26:22', NULL),
(27, 'FTP Accounts', 1, '2021-12-07 10:26:22', NULL),
(28, 'Email / MySQL 5', 1, '2021-12-07 10:26:22', NULL),
(30, 'Kapasitas', 2, '2021-12-07 10:26:22', NULL),
(31, 'Data Transfer (DN)', 2, '2021-12-07 10:26:22', NULL),
(32, 'Email POP', 2, '2021-12-07 10:26:22', NULL),
(33, 'Minimum Kontrak', 2, '2021-12-07 10:26:22', NULL),
(34, 'Email Accounts', 2, '2021-12-07 10:26:22', NULL),
(35, 'FTP Accounts', 2, '2021-12-07 10:26:22', NULL),
(36, 'Reseller Feature', 2, '2021-12-07 10:26:22', NULL),
(37, 'Per Month', 2, '2021-12-07 10:26:22', NULL),
(38, 'Exclude', 2, '2021-12-07 10:26:22', NULL),
(40, 'Setup', 3, '2021-12-07 10:26:22', NULL),
(41, 'Kapasitas', 3, '2021-12-07 10:26:22', NULL),
(42, 'RAM', 3, '2021-12-07 10:26:22', NULL),
(43, 'OS', 3, '2021-12-07 10:26:22', NULL),
(44, 'IP Publik', 3, '2021-12-07 10:26:22', NULL),
(45, 'Per Month', 3, '2021-12-07 10:26:22', NULL),
(46, 'Exclude', 3, '2021-12-07 10:26:22', NULL),
(47, 'Data Center', 4, '2021-12-07 10:26:22', NULL),
(48, 'IP Publik', 4, '2021-12-07 10:26:22', NULL),
(49, 'IIX', 4, '2021-12-07 10:26:22', NULL),
(50, 'International', 4, '2021-12-07 10:26:22', NULL),
(51, 'Power', 4, '2021-12-07 10:26:22', NULL),
(52, 'Size Server', 4, '2021-12-07 10:26:22', NULL),
(53, 'Setup/Instalasi', 4, '2021-12-07 10:26:22', NULL),
(54, 'Per Month', 4, '2021-12-07 10:26:22', NULL),
(55, 'Exclude', 4, '2021-12-07 10:26:22', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_product`
--

CREATE TABLE `tb_product` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(20) NOT NULL,
  `product_desc` text NOT NULL,
  `product_list` text NOT NULL,
  `product_discount` decimal(10,0) DEFAULT NULL,
  `product_price` decimal(10,0) NOT NULL,
  `product_limit` varchar(10) NOT NULL,
  `product_link` text DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_product`
--

INSERT INTO `tb_product` (`product_id`, `category_id`, `product_name`, `product_desc`, `product_list`, `product_discount`, `product_price`, `product_limit`, `product_link`, `created_date`, `updated_date`) VALUES
(1, 1, 'Bayi', '<b>938</b> Pengguna Terdaftar', '<b>0.5X RESOURCE POWER </b>,\n<b>500 MB</b> Disk Space,\n<b>Unlimited</b> Bandwith,\n<b>Unlimited</b> Databases,\n<b>1</b> Domain,\n<b>Instant</b> Backup,\n<b>Unlimited SSL</b> Gratis Selamanya', '14900', '19900', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(2, 1, 'Pelajar', '<b>4.168</b> Pengguna Terdaftar', '<b>1X RESOURCE POWER </b>,\n<b>Unlimited</b> Disk Space,\n<b>Unlimited</b> Bandwith,\n<b>Unlimited</b> POP3 Email,\n<b>Unlimited</b> Databases,\n<b>10</b> Addon Domains,\n<b>Instant</b> Backup,\n<b>Domain Gratis</b> Selamanya,\n<b>Unlimited SSL</b> Gratis Selamanya', '23450', '46900', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(3, 1, 'Personal', '<b>10.017</b> Pengguna Terdaftar', '<b>2X RESOURCE POWER </b>,\n<b>Unlimited</b> Disk Space,\n<b>Unlimited</b> Bandwith,\n<b>Unlimited</b> POP3 Email,\n<b>Unlimited</b> Databases,\n<b>Unlimited</b> Addon Domains,\n<b>Instant</b> Backup,\n<b>Domain Gratis</b> Selamanya,\n<b>Unlimited SSL</b> Gratis Selamanya,\n<b>Private</b> Name Server,\n<b>SpamAssasin</b> Mail Protection,', '38900', '58900', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(4, 1, 'Bisnis', '<b>3.552</b> Pengguna Terdaftar', '<b>3X RESOURCE POWER </b>,\n<b>Unlimited</b> Disk Space,\n<b>Unlimited</b> Bandwith,\n<b>Unlimited</b> POP3 Email,\n<b>Unlimited</b> Databases,\n<b>Unlimited</b> Addon Domains,\n<b>Magic Auto</b> Backup & Restore,\n<b>Domain Gratis</b> Selamanya,\n<b>Unlimited SSL</b> Gratis Selamanya,\n<b>Private</b> Name Server,\n<b>SpamAssasin</b> Mail Protection,\n<b>Prioritas</b> Layanan Support,\n<i class=\"fa fa-star\"></i>\n<i class=\"fa fa-star\"></i>\n<i class=\"fa fa-star\"></i>\n<i class=\"fa fa-star\"></i>\n<i class=\"fa fa-star\"></i><br/>\n<b>SpamExpert</b> Pro Mail Protection', '65900', '109900', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, 3, 'Hosting Murah', '<strong> 10 DAYS FREE TRIAL </strong>', '<strong>GRATIS </strong>Setup,\n<strong>500 MB </strong>Kapasitas,\n<strong>UNLIMITED </strong>Data Transfer (DN),\n<strong>UNLIMITED </strong>Email POP,\n<strong>1 Tahun </strong> Minimum Kontrak,\n<strong>Max 15 </strong>Email Accounts, \n<strong>Max 6 </strong>FTP Accounts,\n<strong> IDR 700.000</strong> Per Month,\n<strong>Vat 10% </strong>Exclude', '0', '0', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(6, 3, 'Hosting Indonesia', '<strong> 10 DAYS FREE TRIAL</strong>', '<strong>GRATIS </strong>Setup,\n<strong>1 GB </strong>Kapasitas,\n<strong>UNLIMITED </strong>Data Transfer (DN),\n<strong>UNLIMITED </strong>Email POP,\n<strong>1 Tahun </strong> Minimum Kontrak,\n<strong>UNLIMITED </strong>Email Accounts, \n<strong>UNLIMITED </strong>FTP Accounts,\n<strong>UNLIMITED </strong>Reseller Feature,\n<strong> IDR 1.000.000</strong> Per Month,\n<strong>Vat 10%</strong> Exclude', '0', '0', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(7, 3, 'Hosting Singapura SG', '<strong> 10 DAYS FREE TRIAL </strong>', '<strong>GRATIS </strong>Setup,\n<strong>2 GB </strong>Kapasitas,\n<strong>UNLIMITED </strong>Data Transfer (DN),\n<strong>UNLIMITED </strong>Email POP,\n<strong>1 Tahun </strong> Minimum Kontrak,\n<strong>UNLIMITED </strong>Email Accounts,\n<strong>UNLIMITED </strong>FTP Accounts,\n<strong>UNLIMITED </strong>Reseller Feature,\n<strong> IDR 1.800.000</strong> Per Month,\n<strong>Vat 10%</strong> Exclude', '0', '0', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(8, 3, 'Hosting PHP', '<strong> 10 DAYS FREE TRIAL </strong>', '<strong>GRATIS </strong>Setup,\n<strong>4 GB </strong>Kapasitas,\n<strong>UNLIMITED </strong>Data Transfer (DN),\n<strong>UNLIMITED </strong>Email POP,\n<strong>1 Tahun </strong> Minimum Kontrak,\n<strong>UNLIMITED </strong>Email Accounts, \n<strong>UNLIMITED </strong>FTP Accounts,\n<strong>UNLIMITED </strong>Reseller Feature,\n<strong> IDR 2.200.000</strong> Per Month,\n<strong>Vat 10%</strong> Exclude', '0', '0', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(9, 3, 'Hosting Wordpress', '<strong> 10 DAYS FREE TRIAL </strong>', '<strong>GRATIS </strong>Setup,\n<strong>10 GB </strong>Kapasitas,\n<strong>128 MB </strong>RAM,\n<strong>Linux/UNIX </strong>Operating System,\n<strong>1 Dedicated </strong>IP Publik,\n<strong> IDR 380.000</strong> Per Month,\n<strong>Vat 10%</strong> Exclude', '0', '0', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(10, 3, 'Hosting Laravel', '<strong> 10 DAYS FREE TRIAL </strong>', '<strong>GRATIS </strong>Setup,\n<strong>12 GB </strong>Kapasitas,\n<strong>512 MB </strong>RAM,\n<strong>Linux/UNIX </strong>Operating System,\n<strong>1 Dedicated </strong>IP Publik,\n<strong>IDR 730.000</strong> Per Month ,\n<strong>Vat 10%</strong> Exclude', '0', '0', 'MONTH', ' ', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_register`
--

CREATE TABLE `tb_register` (
  `regis_id` int(11) NOT NULL,
  `regis_status` tinyint(4) NOT NULL,
  `category_id` text NOT NULL,
  `regis_name` varchar(100) NOT NULL,
  `regis_hukum` varchar(50) NOT NULL,
  `regis_jabatan` varchar(50) NOT NULL,
  `regis_usaha` varchar(50) NOT NULL,
  `regis_alamat` text NOT NULL,
  `regis_telp` varchar(20) NOT NULL,
  `regis_fax` varchar(20) DEFAULT NULL,
  `regis_hp` varchar(20) NOT NULL,
  `regis_website` text DEFAULT NULL,
  `regis_whatsapp` varchar(20) DEFAULT NULL,
  `regis_bbm` text DEFAULT NULL,
  `regis_instagram` text DEFAULT NULL,
  `regis_facebook` text DEFAULT NULL,
  `regis_youtube` text DEFAULT NULL,
  `regis_image` text NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `regis_email` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_register`
--

INSERT INTO `tb_register` (`regis_id`, `regis_status`, `category_id`, `regis_name`, `regis_hukum`, `regis_jabatan`, `regis_usaha`, `regis_alamat`, `regis_telp`, `regis_fax`, `regis_hp`, `regis_website`, `regis_whatsapp`, `regis_bbm`, `regis_instagram`, `regis_facebook`, `regis_youtube`, `regis_image`, `created_date`, `updated_date`, `regis_email`) VALUES
(5, 1, '4,6,13', 'Percobaan 1', 'Percobaan 1', 'Percobaan 1', 'Percobaan 1', 'Test', '(111) 1111-1111', '(111) 1111-1111', '+11-1111-1111-11111', 'test', '+11-1111-1111-1111', '11111111111', '111111111111111', '111111111111', '1111111111', '14157708582.jpg,1415770858.jpg,14157708581.jpg,14157708583.JPG', '2021-12-07 10:26:22', '2021-12-07 10:26:22', 'rezamuzay@gmail.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_service`
--

CREATE TABLE `tb_service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(30) NOT NULL,
  `service_image` varchar(255) NOT NULL,
  `service_desc` text NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_service`
--

INSERT INTO `tb_service` (`service_id`, `service_name`, `service_image`, `service_desc`, `created_date`, `updated_date`) VALUES
(1, 'PHP Semua Versi', '/svg/icon PHP Hosting_PHP Semua Versi.svg', 'Pilih mulai dari versi PHP 5.3 s/d PHP 7.<br/>Ubah sesuka Anda!', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(2, 'MySQL Versi 5.6', '/svg/icon PHP Hosting_My SQL.svg', 'Nikmati MySQL versi terbaru, tercepat, dan kaya akan fitur.', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(3, 'Panel Hosting CPanel', '/svg/icon PHP Hosting_CPanel.svg', 'Kelola website dengan panel canggih yang familiar di hati Anda.', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(4, 'Garansi Uptime 99.9%', '/svg/icon PHP Hosting_garansi uptime.svg', 'Data center yang mendukung kelangsungan website Anda 24/7.', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, 'Database InnoDB Unlimited', '/svg/icon PHP Hosting_InnoDB.svg', 'Jumlah dan ukuran database yang tumbuh sesuai kebutuhan Anda.', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(6, 'Wildcard Remote MySQL', '/svg/icon PHP Hosting_My SQL remote.svg', 'Mendukung s/d 25 max_user_connections dan 100 max_connections.', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_socmed`
--

CREATE TABLE `tb_socmed` (
  `socmed_id` int(11) NOT NULL,
  `socmed_name` varchar(50) NOT NULL,
  `socmed_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_socmed`
--

INSERT INTO `tb_socmed` (`socmed_id`, `socmed_name`, `socmed_value`) VALUES
(1, 'facebook', '#'),
(2, 'twitter', '#'),
(3, 'gplus', '#'),
(4, 'linkedin', '#'),
(5, 'youtube', '#'),
(6, 'dribble', '#'),
(7, 'pinterest', '#'),
(8, 'git', '#');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_tags`
--

CREATE TABLE `tb_tags` (
  `tags_id` int(11) NOT NULL,
  `tags_name` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_tags`
--

INSERT INTO `tb_tags` (`tags_id`, `tags_name`, `created_date`, `updated_date`) VALUES
(1, 'Networking', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(2, 'Internet', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_team`
--

CREATE TABLE `tb_team` (
  `team_id` int(11) NOT NULL,
  `team_name` varchar(50) NOT NULL,
  `team_title` varchar(50) NOT NULL,
  `team_image` text NOT NULL,
  `team_facebook` text DEFAULT NULL,
  `team_twitter` text DEFAULT NULL,
  `team_gplus` text DEFAULT NULL,
  `team_linkedin` text DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_team`
--

INSERT INTO `tb_team` (`team_id`, `team_name`, `team_title`, `team_image`, `team_facebook`, `team_twitter`, `team_gplus`, `team_linkedin`, `created_date`, `updated_date`) VALUES
(13, 'Muhammad Reza Hardiansyah', 'Programmer Web', '4019821297.png', 'https://www.facebook.com/M.Reza.Hardiansyah', NULL, NULL, NULL, '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_testimonial`
--

CREATE TABLE `tb_testimonial` (
  `testimonial_id` int(11) NOT NULL,
  `testimonial_name` varchar(50) NOT NULL,
  `testimonial_title` varchar(50) NOT NULL,
  `testimonial_company` varchar(100) NOT NULL,
  `testimonial_image` text NOT NULL,
  `testimonial_desc` text DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_title`
--

CREATE TABLE `tb_title` (
  `title_id` int(11) NOT NULL,
  `title_name` varchar(10) NOT NULL,
  `title_alias` text NOT NULL,
  `title_desc` text NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_title`
--

INSERT INTO `tb_title` (`title_id`, `title_name`, `title_alias`, `title_desc`, `created_date`, `updated_date`) VALUES
(1, 'phplimit', 'Powerful dengan Limit PHP yang lebih Besar', 'max execution time 300s, max execution time 300s, php memory limit 1024 MB, post max size 128 MB, upload max filesize 128 MB, max input vars 2500', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(2, 'paket', 'Semua Paket Hosting Sudah Termasuk', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(3, 'framework', 'Mendukung Penuh Framework Laravel', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(4, 'features', 'Modul Lengkap untuk Menjalankan Apliksai PHP Anda.', 'IcePHP,apac,apcu,apm,ares,bcmath,bcompiler,big_int,bitset,bloomy,bz2_filter,clamac,coin_acceptor,crack,dba,http,huffman,idn,igbinary,imagick,imap,inclued,inotify,interbace,intl,ioncube_loader,ioncube_loader_4,jsmin,json,ldap,nd_pdo_mysql,oauth,oci8,odbc,opcache,pdf,pdo,pdo_dblib,pdo_firebird,pdo_mysql,pdo_odbc,pdo_pgsql,pdo_sqlite,pgsql,phalcon,stats,stem,stomp,suhosin,sybase_ct,sysvmsg,sysvsem,sysvshm,tidy,timezonedb,trade,transit,uploadprogress,uri_template,uuid', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(5, 'faq', 'Linux Hosting yang Stabil dengan Teknologi LVE', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(6, 'pricing', 'Our Team', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(7, 'client', 'Our Client', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(8, 'domain', 'Domain Price', 'Program Satu Juta Nama Domain merupakan salah satu program unggulan Kementerian Komunikasi dan Informatika Republik Indonesia untuk meningkatkan konten-konten positif dan produktif di Internet.', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(9, 'about', 'About Details', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(10, 'contact', '	\r\nContact Page', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(11, 'testimoni', 'Feedback Form', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(12, 'login', 'Login Page', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(13, 'blog', 'Blog Page', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(14, 'page', 'Page', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22'),
(15, 'details', 'Price Details', '', '2021-12-07 10:26:22', '2021-12-07 10:26:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 1,
  `banned` tinyint(1) NOT NULL DEFAULT 0,
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `role` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`, `role`) VALUES
(1, 'admin', '$2a$08$BXw8YE0ycU65ZoTbbu6ozebqxIQTGNrsSXqqAq0EwHvbgk9OdjqPi', 'rezamuzay@gmail.com', 1, 0, '', NULL, NULL, NULL, NULL, '127.0.0.1', '2021-11-30 05:12:15', '2015-12-26 07:13:14', '2021-11-30 04:12:15', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_autologin`
--

CREATE TABLE `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indeks untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_activity`
--
ALTER TABLE `tb_activity`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indeks untuk tabel `tb_article`
--
ALTER TABLE `tb_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indeks untuk tabel `tb_categories`
--
ALTER TABLE `tb_categories`
  ADD PRIMARY KEY (`categories_id`);

--
-- Indeks untuk tabel `tb_category`
--
ALTER TABLE `tb_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indeks untuk tabel `tb_client`
--
ALTER TABLE `tb_client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indeks untuk tabel `tb_contact`
--
ALTER TABLE `tb_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indeks untuk tabel `tb_cp`
--
ALTER TABLE `tb_cp`
  ADD PRIMARY KEY (`cp_id`);

--
-- Indeks untuk tabel `tb_domain`
--
ALTER TABLE `tb_domain`
  ADD PRIMARY KEY (`domain_id`);

--
-- Indeks untuk tabel `tb_faq`
--
ALTER TABLE `tb_faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indeks untuk tabel `tb_feedback`
--
ALTER TABLE `tb_feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indeks untuk tabel `tb_image`
--
ALTER TABLE `tb_image`
  ADD PRIMARY KEY (`image_id`);

--
-- Indeks untuk tabel `tb_info`
--
ALTER TABLE `tb_info`
  ADD PRIMARY KEY (`info_id`);

--
-- Indeks untuk tabel `tb_join`
--
ALTER TABLE `tb_join`
  ADD PRIMARY KEY (`join_id`);

--
-- Indeks untuk tabel `tb_member`
--
ALTER TABLE `tb_member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indeks untuk tabel `tb_message`
--
ALTER TABLE `tb_message`
  ADD PRIMARY KEY (`message_id`);

--
-- Indeks untuk tabel `tb_news`
--
ALTER TABLE `tb_news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indeks untuk tabel `tb_newsletter`
--
ALTER TABLE `tb_newsletter`
  ADD PRIMARY KEY (`newsletter_id`);

--
-- Indeks untuk tabel `tb_page`
--
ALTER TABLE `tb_page`
  ADD PRIMARY KEY (`page_id`);

--
-- Indeks untuk tabel `tb_price`
--
ALTER TABLE `tb_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Indeks untuk tabel `tb_product`
--
ALTER TABLE `tb_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indeks untuk tabel `tb_register`
--
ALTER TABLE `tb_register`
  ADD PRIMARY KEY (`regis_id`);

--
-- Indeks untuk tabel `tb_service`
--
ALTER TABLE `tb_service`
  ADD PRIMARY KEY (`service_id`);

--
-- Indeks untuk tabel `tb_socmed`
--
ALTER TABLE `tb_socmed`
  ADD PRIMARY KEY (`socmed_id`);

--
-- Indeks untuk tabel `tb_tags`
--
ALTER TABLE `tb_tags`
  ADD PRIMARY KEY (`tags_id`);

--
-- Indeks untuk tabel `tb_team`
--
ALTER TABLE `tb_team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indeks untuk tabel `tb_testimonial`
--
ALTER TABLE `tb_testimonial`
  ADD PRIMARY KEY (`testimonial_id`);

--
-- Indeks untuk tabel `tb_title`
--
ALTER TABLE `tb_title`
  ADD PRIMARY KEY (`title_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_autologin`
--
ALTER TABLE `user_autologin`
  ADD PRIMARY KEY (`key_id`,`user_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_activity`
--
ALTER TABLE `tb_activity`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_article`
--
ALTER TABLE `tb_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `tb_categories`
--
ALTER TABLE `tb_categories`
  MODIFY `categories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_category`
--
ALTER TABLE `tb_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_client`
--
ALTER TABLE `tb_client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tb_contact`
--
ALTER TABLE `tb_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_cp`
--
ALTER TABLE `tb_cp`
  MODIFY `cp_id` int(11) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_domain`
--
ALTER TABLE `tb_domain`
  MODIFY `domain_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tb_faq`
--
ALTER TABLE `tb_faq`
  MODIFY `faq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tb_feedback`
--
ALTER TABLE `tb_feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_image`
--
ALTER TABLE `tb_image`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_info`
--
ALTER TABLE `tb_info`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tb_join`
--
ALTER TABLE `tb_join`
  MODIFY `join_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT untuk tabel `tb_member`
--
ALTER TABLE `tb_member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_message`
--
ALTER TABLE `tb_message`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_news`
--
ALTER TABLE `tb_news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tb_newsletter`
--
ALTER TABLE `tb_newsletter`
  MODIFY `newsletter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_page`
--
ALTER TABLE `tb_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_price`
--
ALTER TABLE `tb_price`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT untuk tabel `tb_product`
--
ALTER TABLE `tb_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `tb_register`
--
ALTER TABLE `tb_register`
  MODIFY `regis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tb_service`
--
ALTER TABLE `tb_service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tb_socmed`
--
ALTER TABLE `tb_socmed`
  MODIFY `socmed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tb_tags`
--
ALTER TABLE `tb_tags`
  MODIFY `tags_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_team`
--
ALTER TABLE `tb_team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tb_testimonial`
--
ALTER TABLE `tb_testimonial`
  MODIFY `testimonial_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tb_title`
--
ALTER TABLE `tb_title`
  MODIFY `title_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
