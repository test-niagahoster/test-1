## Requirements

* PHP 7.4 (stable)
* Composer
* MySQL / MariaDB

## Installation

~~~
$ import database niagahoster to local
$ cd test-1
$ composer install
~~~

## How to Run

~~~
$ cd test-1
$ bin/server.sh
~~~

Access `http://127.0.0.1:8000/`.

## References

* [CodeIgniter Twig Samples](https://github.com/kenjis/codeigniter-twig-samples)

## Test

Please create a new PHP page from a given mockup. Just a single landing page.

The mockup file is located on https://github.com/bylabz/frontend-test/blob/master/assets/images/mockup.png or you can simply clone this GitHub repository (https://github.com/bylabz/frontend-test.git) into your local machine.
Feel free to use your favourite CSS framework, bootstrap, materialize CSS, and etc.
All the icons and the other images you need should be available on that GitHub repository. https://github.com/bylabz/frontend-test/tree/master/assets
You should use the same fonts as the mockup needed. For example, this section title (http://i.prntscr.com/96t7lWY_RNSAP_8YKbd1hw.png) must be Montserrat-Bold.
Please don't put a hardcoded value of the price (http://i.prntscr.com/nuCmy2U3Trm2ip7wimzDXA.png). You can store the price value on database or JSON data, so the price will be dynamic.
And for sure, take care of the mobile view. Make sure that your page does not break on the mobile device resolution.
Finally, please give us your page on both preview URL and link to your code repository.

Note: if you are familiar with the twig, using twig on creating your preview page is much better.