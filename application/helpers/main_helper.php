<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  if ( ! function_exists('uri')){
    function uri($uri){
      $CI =& get_instance();
      return $CI->uri->segment($uri);
    }
  }

// GET DATA SERVER

  if ( ! function_exists('information')){
    function information(){
      $CI =& get_instance();
      $information = $CI->crud->readData('*', 'tb_info', array(), array(), '', 'info_id', 'ASC');
      return $information;
    }
  }
  if ( ! function_exists('contact')){
    function contact(){
      $CI =& get_instance();
      $contact = $CI->crud->readData('*', 'tb_contact', array(), array(), '', 'contact_id', 'ASC');
      return $contact;
    }
  }
  if ( ! function_exists('socmed')){
    function socmed(){
      $CI =& get_instance();
      $socmed = $CI->crud->readData('*', 'tb_socmed', array(), array(), '', 'socmed_id', 'ASC');
      return $socmed;
    }
  }
  if ( ! function_exists('image')){
    function image(){
      $CI =& get_instance();
      $image = $CI->crud->readData('*', 'tb_image', array(), array(), '', 'image_id', 'ASC');
      return $image;
    }
  }
  if ( ! function_exists('title')){
    function title(){
      $CI =& get_instance();
      $table = $CI->crud->readData('*', 'tb_title', array(), array(), '', 'title_name', 'ASC');
      return $table;
    }
  }
  if ( ! function_exists('cp')){
    function cp(){
      $CI =& get_instance();
      $table = $CI->crud->readData('*', 'tb_cp', array(), array(), '', 'cp_name', 'ASC');
      return $table;
    }
  }
  if ( ! function_exists('page')){
    function page(){
      $CI =& get_instance();
      $table = $CI->crud->readData('*', 'tb_page', array(), array(), '', 'page_order', 'ASC');
      return $table;
    }
  }

  /**
   * [file_name | File Name]
   * @return [array] [data]
   */
  if ( ! function_exists('file_name')){
    function file_name(){
      return crc32(date('Ymd'));
    }
  }
// ------------------------------------------------------------------------

/**
 * CSS URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access  public
 * @param string
 * @return  string
 */

if( ! function_exists('css_url'))
{
  function css_url($uri = '')
  {
    return base_url().'/css/';
  }
}
// ------------------------------------------------------------------------

/**
 * JS URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access  public
 * @param string
 * @return  string
 */

if( ! function_exists('js_url'))
{
  function js_url($uri = '')
  {
    return base_url().'/js/';
  }
}

/**
 * Icon Web
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access  public
 * @param string
 * @return  string
 */

if( ! function_exists('icon_url'))
{
  function icon_url($uri = '')
  {
    return base_url().'/icon/';
  }
}

// ------------------------------------------------------------------------

/**
 * Plugins URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access  public
 * @param string
 * @return  string
 */

if( ! function_exists('plug_url'))
{
  function plug_url($uri = '')
  {
    return base_url().'/plugins/';
  }
}

// ------------------------------------------------------------------------

/**
 * Images URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access  public
 * @param string
 * @return  string
 */

if( ! function_exists('img_url'))
{
  function img_url($uri = '')
  {
    return  base_url().'svg/';
  }
}

if( ! function_exists('img_alias'))
{
  function img_alias($uri = '')
  {
    return base_url().'/images/';
  }
}

if( ! function_exists('upload_url'))
{
  function upload_url($uri = '')
  {
    return base_url().'/upload/';
  }
}
if ( ! function_exists('pagination')){
  function pagination($per_page, $total_rows, $keyword=null){
    $CI =& get_instance();

    $config['per_page']         = $per_page;
    $config['total_rows']       = $total_rows;
    $config['base_url']         = base_url().$CI->uri->segment(1).'/'.$CI->uri->segment(2).'/'.$CI->uri->segment(3).'?page='.$per_page.$keyword;
    $config['full_tag_open']    = '<ul class="pagination">';
    $config['full_tag_close']   = '</ul>';
    $config['first_link']       = '<i class="fa fa-angle-double-left"></i>';
    $config['last_link']        = '<i class="fa fa-angle-double-right"></i>';
    $config['first_tag_open']   = '<li>';
    $config['first_tag_close']  = '</li>';
    $config['prev_link']        = '<i class="fa fa-angle-left"></i>';
    $config['prev_tag_open']    = '<li>';
    $config['prev_tag_close']   = '</li>';
    $config['next_link']        = '<i class="fa fa-angle-right"></i>';
    $config['next_tag_open']    = '<li>';
    $config['next_tag_close']   = '</li>';
    $config['last_tag_open']    = '<li>';
    $config['last_tag_close']   = '</li>';
    $config['cur_tag_open']     = '<li class="active"><a href="#">';
    $config['cur_tag_close']    = '</a></li>';
    $config['num_tag_open']     = '<li>';
    $config['num_tag_close']    = '</li>';
    $config['use_page_numbers'] = FALSE;
    $config['page_query_string']= TRUE;
    $config["num_links"]        = 5;

    $CI->pagination->initialize($config);
    return $CI->pagination->create_links();
  }
}
/* End of file main_helper.php */
/* Location: ./application/helpers/main_helper.php */
/* Created By Reza_Muzay */
?>
