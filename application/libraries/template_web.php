<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Template_Web{
    protected $_ci;

    function __construct(){
        $this->_ci = &get_instance();
    }

    function main($content, $data = NULL){
        $data['header']     = $this->_ci->load->view('header', $data, TRUE);
        $data['navbar']     = $this->_ci->load->view('navbar', $data, TRUE);
        $data['content']    = "";
        foreach ($content as $key => $value) {
          $data['content']  .= $this->_ci->load->view($value, $data, TRUE);
        }
        $data['footer']     = $this->_ci->load->view('footer', $data, TRUE);

        $this->_ci->load->view('index', $data);
    }
}

/* End of file Template_web.php */
/* Location: ./application/libraries/Template_web.php */
/* Created By Reza_Muzay */
