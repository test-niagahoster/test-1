        <div class="container">
            <div class="row text-center title">
                <h2><?php echo $title_alias_phplimit; ?></h2>
                <div class="banner--content">
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tbody>
                            <?php
                            $list = explode(',',$title_desc_phplimit);
                            $i = 0;
                            for($i=0;$i<3;$i++){
                                echo "<tr>";
                                echo "<td>".$list[$i]."</td>";
                                echo "</tr>";
                            }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-striped">
                            <tbody>
                            <?php
                            $list = explode(',',$title_desc_phplimit);
                            $i = 0;
                            for($i=3;$i<6;$i++){
                                echo "<tr>";
                                echo "<td>".$list[$i]."</td>";
                                echo "</tr>";
                            }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row text-center title">
                <h2 style="margin-bottom:-40px"><?php echo $title_alias_paket; ?></h2>
                <?php
                    foreach($features as $col){ ?>
                       <div class="col-md-4" style="margin-top:90px">
                            <img src="<?php echo base_url().$col['service_image']; ?>" width="60px!important"><br/>
                            <h3><b> <?php echo $col['service_name']; ?> </b></h3>
                            <p> <?php echo $col['service_desc']; ?></p>

                       </div>
                <?php    }  ?>
            </div>
            <div class="row">
                <h2 style="margin-bottom:-40px" class="text-center title"><?php echo  $article[1]['article_name']; ?></h2><br/></br>
                <div class="col-md-6">
                    <div class="banner--content">
                    <?php
                        echo $article[1]['article_summary'];
                        $list = explode(',',$article[1]['article_content']);
                        echo "<ul>";
                        foreach ($list as $row):
                            echo "<li>".$row."</li>";
                        endforeach;
                        echo "</ul>";
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo $article[1]['article_image'];?>">
                </div>
            </div>
            <div class="row">
                <h2 class="text-center"><?php echo $title_alias_features; ?></h2><br/></br>
                <?php 
                $list = explode(',',$title_desc_features);
                $i = 0;
                foreach($list as $col){
                    if($i == 0) echo '<div class="col-md-3">';
                    echo $col.'<br/>';

                    if($i == 14){
                        echo '</div>';
                        $i = 0;
                    }else{
                        $i++;
                    } 
                }
                ?>
                <div class="text-center"><div class="pricing--footer">
                <br/></br/>
                
                                    <a href="" class="btn--primary" style="margin-top:30px">Selengkapnya</a>
                                </div></div>
            </div>
            <div class="row">
                <h2 style="margin-bottom:-40px" class="text-center title"><?php echo  $article[2]['article_name']; ?></h2><br/></br>
                    <div class="col-md-6">
                        <div class="banner--content">
                        <?php
                            echo $article[2]['article_content'];
                            
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="<?php echo $article[2]['article_image'];?>">
                    </div>
            </div>
        </div>