        <div id="pricing">
            <div class="container">
                <div class="row reset-gutter">
                  <?php
                      for ($u=0; $u < count($product); $u++):
                        if ($product[$u]['category_id'] == 1):?>
                          <div class="col-md-3 pricing--item-h" style="padding:0px!important">
                            <div class="pricing--item <?php if($u == 2) echo "raised" ;?>">
                                <div class="pricing--body">
                                    <div class="pricing--tag">
                                      <h2><strong><?php echo $product[$u]['product_name']; ?></strong></h2><hr/>
                                      <strike> Rp <?php echo number_format($product[$u]['product_price']);?></strike><br/>
                                      <p><span>Rp</span><?php echo '<strong>'.substr(number_format($product[$u]['product_discount']), 0, 2).'</strong><span><b>'.substr(number_format($product[$u]['product_discount']), 2).'</b></span>'; ?></strong><span>/bln</span></p>
                                      <hr/>
                                      <p class="pricing--desc"><?php echo $product[$u]['product_desc']; ?></p>
                                      <hr/>
                                    </div>
                                    <div class="pricing--details">
                                        <?php
                                        $list = explode(',',$product[$u]['product_list']);
                                          foreach ($list as $row):
                                              echo $row."<br/>";
                                          endforeach;
                                       ?>
                                      
                                    </div>
                                </div>
                                <div class="pricing--footer">
                                    <a href="" class="btn--primary"><?php echo ($u == 3) ? "Diskon 40%" : "Pilih Sekarang" ;?></a>
                                </div>
                            </div>
                          </div>
                  <?php
                        endif;
                      endfor;
                  ?>
                </div>
            </div>
        </div>
