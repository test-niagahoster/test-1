        <div class="container">
            <div class="row">
                <div class="col-md-4 text-center">
                    <span class="img-module">
                        <embed src="<?php echo img_url().$image_zenguard;?>">
                    </span>
                    <?php echo $image_desc_zenguard; ?>
                </div>
                <div class="col-md-4 text-center">
                    <span class="img-module">
                        <embed src="<?php echo img_url().$image_composer;?>">
                    </span>
                    <?php echo $image_desc_composer; ?>
                </div>
                <div class="col-md-4 text-center">
                    <span class="img-module">
                        <embed src="<?php echo img_url().$image_ioncube;?>">
                    </span>
                    <?php echo $image_desc_ioncube; ?>
                </div>
            </div>
        </div>
        <div class="container text-center title">
            <div class="row">
                <h1><strong>Paket Hosting Singapura yang Tepat</strong></h1>
                <h2>Diskon 40% + Domain dan SSL Gratis untuk Anda</h2>
            </div>
        </div>