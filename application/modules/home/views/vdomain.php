<?php $image = image();?>
<!-- Domain Search Area Start -->
<style type="text/css">
    #domainSearch:before {
        content: " ";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        width: 1170px;
        height: 100%;
        margin: 0 auto;
        background-image: url(<?php echo img_url().'/info/'.$image[2]['image_value'];?>);
        background-repeat: no-repeat;
        background-position: 100% 100%;
    }
</style>
        <div id="domainSearch" class="bg--whitesmoke">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <!-- Section Title Start -->
                        <div class="section--title block">
                            <h2>Looking A Premium Quality<span>Domain Name?</span></h2>
                        </div>
                        <!-- Section Title End -->
                        <!-- Domain Search Form Start -->
                        <div class="domain-search--form">
                            <form action="https://member.newtonindonesia.co.id/domainchecker.php?systpl=HostWHMCScv1" method="post">
                                <div class="input--text">
                                    <input type="text" name="domain" placeholder="eg. example" class="form-control">
                                    <span class="highlight"></span>
                                </div>

                                <div class="input--radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="ext" value=".com" checked="checked">
                                        <span>.com</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="ext" value=".net">
                                        <span>.net</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="ext" value=".org">
                                        <span>.org</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="ext" value=".info">
                                        <span>.info</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="ext" value=".biz">
                                        <span>.biz</span>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="ext" value=".us">
                                        <span>.us</span>
                                    </label>
                                </div>

                                <button type="submit" class="btn--primary btn--ripple">Search</button>
                            </form>
                        </div>
                        <!-- Domain Search Form End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Domain Search Area End -->
