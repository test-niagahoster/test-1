        <div id="services">
            <div class="container">
                <div class="section--title">
                    <div class="row">
                        <div class="col-md-6">
                          <?php
                            $title_name = explode(' ',$title_alias_phplimit);
                            echo "<h2>".$title_name[0];
                            if($title_name[1]) echo " <span>".$title_name[1]."</span>";
                            echo "</h2>";
                           ?>
                        </div>
                        <div class="col-md-6">
                        <?php echo "<p>".$title_desc_service."</p>"; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <?php foreach ($category as $row) : ?>
                    <div class="col-md-6 service--item-h">
                        <div class="service--item">
                            <div class="service--icon">
                                <img src="<?php echo img_url().'/category/'.$row['category_image'];?>" alt="">
                            </div>
                            <div class="service--content">
                            <?php
                              $category_name = explode(' ',$row['category_name']);
                              echo "<h3>".$category_name[0];
                              if($category_name[1]) echo " <span>".$category_name[1]."</span>";
                              echo "</h3>";

                              echo "<p>".$row['category_desc']."</p>";
                            ?>
                            </div>
                        </div>
                    </div>
                  <?php endforeach;?>
                </div>
            </div>
        </div>