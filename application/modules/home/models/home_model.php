<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	private $table = "tb_info";
	private $order = "info_title";
	private $field = "info_id,info_title,info_content";

	function search($keyword){
		$this->db->like('info_title', $keyword);
		$this->db->or_like('info_content', $keyword);
	}
	function data_total($keyword=null){
		($keyword) ? $this->search($keyword) : '';
		$this->db->select($this->field);

		$this->db->order_by($this->order, "ASC");
		return $this->db->get($this->table)->num_rows();
	}
	function data($keyword=null,$num,$offset){
		($keyword) ? $this->search($keyword) : '';
		$this->db->select($this->field);
		$this->db->limit($num, $offset);
		$this->db->order_by($this->order, "ASC");
		return $this->db->get($this->table)->result();
	}

}
