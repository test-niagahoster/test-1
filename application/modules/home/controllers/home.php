<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
        $this->load->library('template_web');
	}

	function index(){
		foreach (image() as $value):
			$data['image_'.$value['image_name']] = $value['image_value'];
			$data['image_desc_'.$value['image_name']] = $value['image_desc'];
		endforeach;
		foreach (title() as $value):
			$data['title_alias_'.$value['title_name']] = $value['title_alias'];
			$data['title_desc_'.$value['title_name']]  = $value['title_desc'];
		endforeach;
		$data['article'] 		= $this->crud->readData('*','tb_article',array(),array(),'','article_id','ASC');
		$data['menu'] 			= $this->crud->readData('*','tb_category',array(),array(),'','category_id','ASC');
		$data['category'] 		= $this->crud->readData('*','tb_category',array(),array(),'','category_id','ASC');
		$data['product'] 		= $this->crud->readData('*','tb_product',array(),array(),'','product_id','ASC');
		$data['feedback'] 		= $this->crud->readData('*','tb_testimonial',array(),array(),'','testimonial_id','ASC');
		$data['features']		= $this->crud->readData('*','tb_service',array(),array(),'','service_id','ASC');
		$data['categories']		= $this->crud->readData('*','tb_categories',array(),array(),'','categories_id','ASC');
		$content = array(
			uri(2).'/vbanner',
			uri(2).'/vmodule',
			uri(2).'/vpricing',
			uri(2).'/vontent',
		);
		$this->template_web->main($content,$data);
	}

	function newsletter(){
		$this->form_validation->set_rules('email', 'email', 'trim|xss_clean|valid_email|required');
		if ($this->form_validation->run()) {
			$data = array(
				'newsletter_email' 	=> $this->form_validation->set_value('email'),
				'created_date'		=> date("Y-m-d H:i:s")
			);

			$this->crud->createData('tb_newsletter', $data);
			echo "Thank You!";
		}else{
			echo validation_errors();
		}

	}
}
