<!DOCTYPE html>
<html lang="en">
  <head>

    <?php echo $header; ?>

  </head>
  <body>
    <div class="wrapper">
      <?php echo $navbar; ?>

      <?php echo $content; ?>

      <?php echo $footer; ?>

  </body>
</html>
