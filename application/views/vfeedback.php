<!-- Feedback Area Start -->
        <div id="feedback" class="bg--whitesmoke">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <div class="row">
                        <div class="col-md-6">
                          <?php
                            // Title Service
                            $title_name = explode(' ',$title_alias_feedback);
                            echo "<h2>".$title_name[0];
                            if($title_name[1]) echo " <span>".$title_name[1]."</span>";
                            echo "</h2>";
                           ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo "<p>".$title_desc_feedback."</p>"; ?>
                        </div>
                    </div>
                </div>
                <!-- Section Title End -->
                <div class="feedback--nav-tabs">
                    <ul class="nav nav-tabs domain-ext--slider">
                      <?php for ($i=0; $i < count($feedback); $i++): ?>
                        <li class="domain-ext--item">
                            <a href="#feedbackTab<?php echo $i;?>" data-toggle="tab" class="domain-ext--content">
                                <img src="<?php echo img_url().'/testimonial/'.$feedback[$i]['testimonial_image'];?>" style="width:180px;height:190px;">
                            </a>
                        </li>
                      <?php endfor; ?>
                    </ul>
                </div>

                <div class="feedback--tabs">
                    <div class="tab-content">
                      <?php for ($i=0; $i < count($feedback); $i++): ?>
                        <div class="tab-pane fade <?php if($i == 0) echo 'in active';?>" id="feedbackTab<?php echo $i;?>">
                            <b><?php echo $feedback[$i]['testimonial_name'];?></b><br/>
                            <i><?php echo $feedback[$i]['testimonial_title'];?> - <small><?php echo $feedback[$i]['testimonial_company'];?></small></i><br/><br/>
                            <?php echo $feedback[$i]['testimonial_desc'];?>
                        </div>
                      <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Feedback Area End -->
