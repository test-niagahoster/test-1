        <div id="banner">
          <div class="container">
            <div class="row">
              <div class="col-md-6">
                <div class="banner--content">
                <?php
                  echo $article[0]['article_summary'];
                  $list = explode(',',$article[0]['article_content']);
                  echo "<ul>";
                    foreach ($list as $row):
                        echo "<li>".$row."</li>";
                    endforeach;
                  echo "</ul>";
                  ?>
                </div>
              </div>
              <div class="col-md-6">
                <img src="<?php echo $article[0]['article_image'];?>">
              </div>
            </div>
          </div>
        </div>
        <hr/>
        
