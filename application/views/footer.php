        <div id="share">
            <div class="container">
                <div class="col-md-6">
                    <b>Bagikan jika Anda menyukai halaman ini.</b>
                </div>
                <div class="col-md-6">
                    <embed src="<?php echo img_alias().$image_bagikan;?>">
                </div>
            </div>
        </div>
        <div id="contact">
            <div class="container">
                <div class="col-md-8 monstserrat col1">
                    Perlu <b>BANTUAN?</b> Hubungi Kami : <b>0274-5305505</b>
                </div>
                <div class="col-md-4 col2">
                    <a href="" class="btn--primary"><i class="fa fa-comments"></i> Live Chat</a>
                </div>
            </div>
        </div>
        <?php $info = information();?>
        <?php $contact = contact();?>
        <?php $image = image();?>
        <?php $socmed = socmed();?>
        <?php $cp = cp();?>
        <?php $page = page();?>
        <footer id="footer">
            <div class="container">
                <!-- <div class="footer--bg" data-bg-img="<?php echo img_url().'/info/'.$image[1]['image_value'];?>"></div> -->
                <div class="row">
                    <div class="col-md-3 col--footer roboto">
                        <p>HUBUNGI KAMI</p>
                        <span class="desc">
                            <?php echo $info[6]['info_value'];?>
                        </span>
                    </div>
                    <div class="col-md-3 col--footer roboto">
                        <p>LAYANAN</p>
                        <span class="desc">
                            <?php
                            foreach($category as $col):
                                echo $col['category_name']."<br/>";
                            endforeach
                            ?>
                        </span>
                    </div>
                    <div class="col-md-3 col--footer roboto">
                        <p>SERVICE HOSTING</p>
                        <span class="desc">
                            <?php
                            foreach($product as $col):
                                if($col['category_id'] == 3):
                                    echo $col['product_name']."<br/>";
                                endif;
                            endforeach;
                            ?>
                        </span>
                    </div>
                    <?php
                    foreach($categories as $row){
                        if($row['categories_id'] != 1){  
                            if($row['categories_id'] == 5 || $row['categories_id'] == 9) echo '<div class="row">';
                            echo '<div class="col-md-3 col--footer roboto">';
                            echo '<p>'.$row['categories_name'].'</p>';
                            echo '<span class="desc">';
                            foreach($article as $col){
                                if($col['categories_id'] != 1 && ($row['categories_id'] == $col['categories_id'])){
                                    if($col['categories_id'] > 6){
                                        echo '<img src="'.base_url().$col['article_image'].'"><br/>';
                                    } else {
                                        echo $col['article_name'],"<br/>";
                                    }
                                }
                            }
                            echo '</span>';
                            echo '</div>';
                            if($row['categories_id'] == 4 || $row['categories_id'] == 8 || $row['categories_id'] == 9 ) echo '</div>';
                        }
                    }
                    ?>
            
            
            
            
            
                <div class="footer--copyright">
                    <div class="row">
                        <div class="col-md-8 col--footer-bottom roboto">
                            Copyright ©2016 Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta <br/>
                            Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology
                        </div>
                        <div class="col-md-4 col--footer-bottom roboto text-right">
                            Syarat dan ketentuan | Kebijakan Privasi
                        </div>
                    </div>
                </div>
            </div>
        </footer>