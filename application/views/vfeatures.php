<!-- Features Area Start -->
        <div id="features">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <div class="row">
                        <div class="col-md-6">
                          <?php
                            // Title Service
                            $title_name = explode(' ',$title_alias_features);
                            echo "<h2>".$title_name[0];
                            if($title_name[1]) echo " <span>".$title_name[1]."</span>";
                            echo "</h2>";
                           ?>
                        </div>
                        <div class="col-md-6">
                            <?php echo "<p>".$title_desc_features."</p>"; ?>
                        </div>
                    </div>
                </div>
                <!-- Section Title End -->
                <div class="row">
                  <?php foreach ($features as $value): ?>
                    <div class="col-md-3 feature--item-h">
                        <!-- Feature Item Start -->
                        <div class="feature--item">
                            <div class="feature--icon">
                                <img src="<?php echo img_url().'/service/'.$value['service_image'];?>" alt="" class="img-responsive center-block">
                            </div>
                            <div class="feature--content">
                                

                            </div>
                        </div>
                        <!-- Feature Item End -->
                    </div>
                  <?php endforeach; ?>
                </div>
            </div>
        </div>
        <!-- Features Area End -->
