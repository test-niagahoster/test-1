    <?php $title = ucfirst($this->uri->segment(1)); $info=information(); ?>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title><?php echo $info[3]['info_value']." : ".$title ;?></title>
    <link type="image/ico" href="<?php echo img_url().'/info/'.$info[4]['info_value'];?>" rel="icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Test Niagahoster">
    <meta name="author" content="Reza.Muzay, Reza Muzay, Mureza, RMuzay, rezamuzay">

    <link href="<?php echo css_url();?>font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo css_url();?>bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo css_url();?>responsive-style.css" rel="stylesheet">
    <link href="<?php echo css_url();?>main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo js_url();?>jquery-3.1.0.min.js"></script>
    <script src="<?php echo js_url();?>bootstrap.min.js"></script>